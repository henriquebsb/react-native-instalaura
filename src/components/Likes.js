import React, { Component } from "react";
import { Image, TouchableOpacity, View, Text, StyleSheet } from "react-native";
import proptypes from "prop-types";

export default class Likes extends Component {
  constructor() {
    super();
    this.state = {};
  }

  carregaIcone(likeada) {
    return likeada
      ? require("../../resources/img/s2-checked.png")
      : require("../../resources/img/s2.png");
  }

  exibeLikes(likers) {
    if (likers.length <= 0) return;

    return (
      <Text style={styles.likes}>
        {likers.length} {likers.length > 1 ? "curtidas" : "curtida"}
      </Text>
    );
  }

  render() {
    const { foto, likeCallback } = this.props;

    return (
      <View>
        <TouchableOpacity onPress={() => likeCallback(foto.id)}>
          <Image
            style={styles.botaoDeLike}
            source={this.carregaIcone(foto.likeada)}
          />
        </TouchableOpacity>

        {this.exibeLikes(foto.likers)}
      </View>
    );
  }
}

Likes.propTypes = {
  foto: proptypes.object,
  likeCallback: proptypes.func
};

const styles = StyleSheet.create({
  botaoDeLike: {
    height: 40,
    width: 40
  },
  like: {
    fontWeight: "bold"
  }
});
