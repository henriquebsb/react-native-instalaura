import React, { Component } from "react";
import { FlatList, View, Button, AsyncStorage, ScrollView } from "react-native";
import Post from "./Post";
import proptypes from "prop-types";

import InstaluraFetchService from "../services/InstaluraFetchService";
import Notificacao from "../api/Notificacao";
import HeaderUsuario from "./HeaderUsuario";

// const width = Dimensions.get("screen").width;

export default class Feed extends Component {
  constructor() {
    super();
    this.state = {
      fotos: [],
      fetching: false
    };
  }

  carregaFotos() {
    let uri = "/fotos";

    if (this.props.usuario) uri = `/public/fotos/${this.props.usuario}`;

    InstaluraFetchService.get(uri)
      .then(json => this.setState({ fotos: json }))
      .then(
        this.setState({
          fetching: false
        })
      );
  }

  async componentDidMount() {
    this.props.navigator.setOnNavigatorEvent(evento => {
      if (evento.id === "willAppear") this.carregaFotos();
    });

    // try {
    //   const uri = "http://instalura-api.herokuapp.com/api/fotos";

    //   let token = await AsyncStorage.getItem("token");

    //   let resposta = await fetch(uri, {
    //     headers: new Headers({ "X-AUTH-TOKEN": token })
    //   });

    //   let fotos = await resposta.json();
    //   let fetching = true;
    //   this.setState({ fotos, fetching });
    // } catch (error) {
    //   throw error;
    // }

    // AsyncStorage.getItem("token")
    //   .then(token => {
    //     return {
    //       headers: new Headers({
    //         "X-AUTH-TOKEN": token
    //       })
    //     };
    //   })
    //   .then(requestInfo => fetch(uri, requestInfo))
    //   .then(resposta => resposta.json())
    //   .then(json => this.setState({ fotos: json }));

    // let resposta = await fetch(
    //   "http://instalura-api.herokuapp.com/api/public/fotos/rafael"
    // );

    // let json = await resposta.json();

    // this.setState({
    //   fotos: json
    // });

    // fetch("http://instalura-api.herokuapp.com/api/public/fotos/rafael")
    //   .then(resposta => resposta.json())
    //   .then(json => this.setState({ fotos: json }));
  }

  like = async idFoto => {
    const listaOriginal = this.state.fotos;
    const foto = this.buscaPorId(idFoto);

    AsyncStorage.getItem("usuario")
      .then(usuarioLogado => {
        let novaLista = [];
        // console.warn(usuarioLogado);
        if (!foto.likeada) {
          novaLista = [...foto.likers, { login: usuarioLogado }];
        } else {
          novaLista = foto.likers.filter(liker => {
            return liker.login !== usuarioLogado;
          });
        }

        return novaLista;
      })
      .then(novaLista => {
        const fotoAtualizada = {
          ...foto,
          likeada: !foto.likeada,
          likers: novaLista
        };

        this.atualizaFotos(fotoAtualizada);
      });

    InstaluraFetchService.post(`/fotos/${idFoto}/like`).catch(() => {
      this.setState({ fotos: listaOriginal });
      Notificacao.exibe("Ops...", "Algo deu errado ao curtir.");
    });

    // let novaLista = [];
    // if (!foto.likeada) {
    //   novaLista = [...foto.likers, { login: "meuUsuario" }];
    // } else {
    //   novaLista = foto.likers.filter(liker => {
    //     return liker.login !== "meuUsuario";
    //   });
    // }

    // const fotoAtualizada = {
    //   ...foto,
    //   likeada: !foto.likeada,
    //   likers: novaLista
    // };

    // const fotos = this.atualizaFotos(fotoAtualizada);

    // this.setState({ fotos });

    // let usuarioLogado = await AsyncStorage.getItem("usuario");

    // let novaLista = [];
    // if (!foto.likeada) {
    //   novaLista = [...foto.likers, { login: usuarioLogado }];
    // } else {
    //   novaLista = foto.likers.filter(liker => {
    //     return liker.login !== usuarioLogado;
    //   });
    // }

    // const fotoAtualizada = {
    //   ...foto,
    //   likeada: !foto.likeada,
    //   likers: novaLista
    // };

    // this.atualizaFotos(fotoAtualizada);

    // const uri = `http://instalura-api.herokuapp.com/api/fotos/${idFoto}/like`;

    // let token = await AsyncStorage.getItem("token");

    // fetch(uri, {
    //   method: "POST",
    //   headers: new Headers({
    //     "X-AUTH-TOKEN": token
    //   })
    // });
  };

  adicionaComentario = (idFoto, valorComentario, inputComentario) => {
    if (!valorComentario) return;

    const foto = this.buscaPorId(idFoto);

    // const uri = `http://instalura-api.herokuapp.com/api/fotos/${idFoto}/comment`;

    const comentario = {
      texto: valorComentario
    };

    InstaluraFetchService.post(`/fotos/${idFoto}/comment/`, comentario)
      .then(comentario => [...foto.comentarios, comentario])
      .then(novaLista => {
        const fotoAtualizada = {
          ...foto,
          comentarios: novaLista
        };
        this.atualizaFotos(fotoAtualizada);
        inputComentario.clear();
      });

    // AsyncStorage.getItem("token")
    //   .then(token => {
    //     return {
    //       method: "POST",
    //       body: JSON.stringify({
    //         texto: valorComentario
    //       }),
    //       headers: new Headers({
    //         "Content-type": "application/json",
    //         "X-AUTH-TOKEN": token
    //       })
    //     };
    //   })
    //   .then(reqInfo => fetch(uri, reqInfo))
    //   .then(resposta => resposta.json())
    //   .then(comentario => [...foto.comentarios, comentario])
    //   .then(novaLista => {
    //     const fotoAtualizada = {
    //       ...foto,
    //       comentarios: novaLista
    //     };
    //     this.atualizaFotos(fotoAtualizada);
    //     inputComentario.clear();
    //   });

    // const novaLista = [
    //   ...foto.comentarios,
    //   {
    //     id: valorComentario,
    //     login: "zoinho",
    //     texto: valorComentario
    //   }
    // ];

    // const fotoAtualizada = {
    //   ...foto,
    //   comentarios: novaLista
    // };

    // this.atualizaFotos(fotoAtualizada);

    // this.setState({ fotos });
    // inputComentario.clear();
  };

  buscaPorId = idFoto => {
    return this.state.fotos.find(foto => foto.id === idFoto);
  };

  atualizaFotos = fotoAtualizada => {
    const fotos = this.state.fotos.map(foto =>
      foto.id === fotoAtualizada.id ? fotoAtualizada : foto
    );
    this.setState({
      fotos
    });
  };

  atualizaLista = () => {
    this.setState({
      fetching: true
    });
    this.componentDidMount();
  };

  logout = () => {
    AsyncStorage.removeItem("token");
    AsyncStorage.removeItem("usuario");

    this.props.navigator.showModal({
      screen: "AluraLingua",
      title: "Novidades para você"
    });

    this.props.navigator.resetTo({
      screen: "Login",
      title: "Instalaura Saiu"
    });
  };

  verPerfilUsuario = idFoto => {
    const foto = this.buscaPorId(idFoto);

    // console.warn(idFoto);

    this.props.navigator.push({
      screen: "PerfilUsuario",
      title: foto.loginUsuario,
      backButtonTitle: "",
      passProps: {
        usuario: foto.loginUsuario,
        fotoDePerfil: foto.urlPerfil
        // posts: this.state.fotos.length
      }
    });
  };

  exibeHeader() {
    if (this.props.usuario)
      return (
        <HeaderUsuario
          {...this.props}
          posts={this.state.fotos.length}
          // usuario={this.props.usuario}
          // fotoDePerfil={this.props.fotoDePerfil}
        />
      );
  }

  render() {
    return (
      <ScrollView>
        <Button
          title="Sair"
          onPress={() => {
            this.logout();
          }}
        />
        {this.exibeHeader()}
        {/* <Button
          onPress={() => {
            this.props.navigator.showModal({
              screen: "AluraLingua",
              title: "Novidades para você"
            });
          }}
          title="Abrir modal"
        /> */}
        <FlatList
          style={{ marginTop: 20 }}
          keyExtractor={item => item.id.toString()}
          data={this.state.fotos}
          refreshing={this.state.fetching}
          onRefresh={this.atualizaLista}
          renderItem={({ item }) => (
            <Post
              likeCallback={this.like}
              foto={item}
              verPerfilCallback={this.verPerfilUsuario}
              comentarioCallback={this.adicionaComentario}
            />
          )}
        />
      </ScrollView>
    );
  }
}

Feed.propTypes = {
  navigator: proptypes.object
};
