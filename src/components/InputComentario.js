import React, { Component } from "react";
import {
  TextInput,
  View,
  TouchableOpacity,
  Image,
  StyleSheet
} from "react-native";
import proptypes from "prop-types";

export default class InputComentario extends Component {
  constructor() {
    super();
    this.state = {
      valorComentario: ""
    };
  }

  render() {
    const { comentarioCallback, idFoto } = this.props;

    return (
      <View style={styles.novoComentario}>
        <TextInput
          style={styles.input}
          placeholder="Adicione um comentário..."
          ref={input => (this.inputComentario = input)}
          onChangeText={texto => this.setState({ valorComentario: texto })}
          underlineColorAndroid="transparent"
        />

        <TouchableOpacity
          onPress={() => {
            comentarioCallback(
              idFoto,
              this.state.valorComentario,
              this.inputComentario
            );
            this.setState({ valorComentario: "" });
          }}
        >
          <Image
            style={styles.icone}
            source={require("../../resources/img/send.png")}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

InputComentario.propTypes = {
  comentarioCallback: proptypes.func,
  idFoto: proptypes.number
};

const styles = StyleSheet.create({
  input: {
    height: 40
  },
  icone: {
    height: 30,
    width: 30
  },
  novoComentario: {
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#ddd"
  }
});
