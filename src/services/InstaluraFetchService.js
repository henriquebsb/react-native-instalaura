// import React, {Component } from 'react';
import { AsyncStorage } from "react-native";

export default class InstaluraFetchService {
  static get(recurso) {
    return InstaluraFetchService.makeRequest(recurso);

    // const uri = "http://instalura-api.herokuapp.com/api/" + recurso;

    // return AsyncStorage.getItem("token")
    //   .then(token => {
    //     return {
    //       headers: new Headers({
    //         "X-AUTH-TOKEN": token
    //       })
    //     };
    //   })
    //   .then(requestInfo => fetch(uri, requestInfo))
    //   .then(resposta => resposta.json());
  }

  static post(recurso, dados) {
    return InstaluraFetchService.makeRequest(recurso, "POST", dados);
    // const uri = "http://instalura-api.herokuapp.com/api/" + recurso;

    // return AsyncStorage.getItem("token")
    //   .then(token => {
    //     return {
    //       method: "POST",
    //       body: JSON.stringify(dados),
    //       headers: new Headers({
    //         "Content-type": "application/json",
    //         "X-AUTH-TOKEN": token
    //       })
    //     };
    //   })
    //   .then(requestInfo => fetch(uri, requestInfo))
    //   .then(resposta => resposta.json());
  }

  static makeRequest(recurso, method = "GET", dados) {
    const uri = "https://instalura-api.herokuapp.com/api" + recurso;

    return AsyncStorage.getItem("token")
      .then(token => {
        return {
          method,
          body: JSON.stringify(dados),
          headers: new Headers({
            "content-type": "application/json",
            "X-AUTH-TOKEN": token
          })
        };
      })
      .then(reqInfo => fetch(uri, reqInfo))
      .then(resp => {
        if (resp.ok) return resp.json();
      })
      .catch(error => {
        console.warn(error);
      });
  }
}
