import { Navigation } from "react-native-navigation";
// import { PrimeiraScreen, SegundaScreen } from './App';
import Feed from "./src/components/Feed";
import Login from "./src/screens/Login";
import AluraLingua from "./src/modals/AluraLingua";

import { AsyncStorage } from "react-native";

Navigation.registerComponent("Login", () => Login);
Navigation.registerComponent("Feed", () => Feed);
Navigation.registerComponent("PerfilUsuario", () => Feed);
Navigation.registerComponent("AluraLingua", () => AluraLingua);

AsyncStorage.getItem("token")
  .then(token => {
    if (token) {
      return {
        screen: "Feed",
        title: "Instalaura"
      };
    }

    return {
      screen: "Login",
      title: "Login"
    };
  })
  .then(screen => Navigation.startSingleScreenApp({ screen }));
